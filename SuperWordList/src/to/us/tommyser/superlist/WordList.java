package to.us.tommyser.superlist;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

class WordList {

	private ArrayList<String> wList;
	
	WordList() {
		wList = new ArrayList<>();
	}
	
	public void add(String str) {
		wList.add(str);
		Collections.sort(wList);
	}
	
	public void load(String fileName) {
		try {
			Scanner sc = new Scanner(new File(fileName));
			
			HashSet<String> uniList = new HashSet<>();
			
			wList.clear();
			while(sc.hasNextLine()) {
				uniList.add(sc.nextLine());
			}
			
			wList = new ArrayList<>(uniList);
			Collections.sort(wList);
			
			sc.close();
		} catch (FileNotFoundException e) {
			System.err.println("File " + fileName + " not found.");
		}
	}
	
	public void save(String fileName) {
		try {
			PrintWriter pw = new PrintWriter(fileName);
			
			for(String str: wList) {
				pw.println(str);
			}
			
			pw.close();
		} catch (FileNotFoundException e) {
			System.err.println("File " + fileName + " not found.");
		}
	}
	
	public String[] getFilteredList(Filter pattern) {
		return wList.stream().filter(pattern::matchesFilter).toArray(String[]::new);
	}
	
}

class Filter {
	private static final int PREFIX_MATCH = 0;
	private static final int ANY_MATCH = 1;
	private static final int EXACT_MATCH = 2;

	private boolean ignoreSpace;
	private int matchMode;
	private final String patString;

	Filter(String pattern) {
		matchMode = PREFIX_MATCH;
		ignoreSpace = false;
		patString = pattern;
	}

	public void setExactMatch() {
		matchMode = EXACT_MATCH;
	}

	public void setAnyMatch() {
		matchMode = ANY_MATCH;
	}

	public void setIgnoreSpace() {
		ignoreSpace = true;
	}

	public boolean matchesFilter(String str) {
		String patStr = patString;

		if (ignoreSpace) {
			str = str.replaceAll("\\s+", "");
			patStr = patStr.replaceAll("\\s+", "");
		}

		if(matchMode == PREFIX_MATCH || matchMode == ANY_MATCH)
			patStr = patStr + ".*";

		if(matchMode == ANY_MATCH)
			patStr = ".*" + patStr;

		try {
			return Pattern.matches(patStr, str);
		} catch (PatternSyntaxException e) {
			return false;
		}
	}
}
