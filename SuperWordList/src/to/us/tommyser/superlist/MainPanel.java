package to.us.tommyser.superlist;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class MainPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 538530910647196708L;

	private final WordList wl;
	private JTextField inputField;
	private final JList<String> dispList;
	private JRadioButton exactField, prefField, anyField;
	private JCheckBox spaceField;
	private JLabel logField;

	MainPanel() {

		wl = new WordList();
		wl.load("wordlist.txt");

		setLayout(new BorderLayout());

		JPanel bottomPanel = getBottomPanel();
		add(bottomPanel, BorderLayout.SOUTH);

		JPanel topPanel = getTopPanel();
		add(topPanel, BorderLayout.NORTH);

		dispList = new JList<>();
		dispList.getSelectionModel().addListSelectionListener((event) -> {
			int idx = dispList.getSelectedIndex();
			if (idx == -1) return;
			String str = dispList.getModel().getElementAt(idx);
			StringSelection stringSelection = new StringSelection(str);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(stringSelection, null);
			logField.setText(getTimeString() + "Copied word " + str + " to clipboard.");
		});
		updateDisplay();
		add(new JScrollPane(dispList), BorderLayout.CENTER);

	}

	private JPanel getBottomPanel() {
		JPanel bottomPanel = new JPanel(new BorderLayout());

		logField = new JLabel();
		bottomPanel.add(logField, BorderLayout.NORTH);

		JPanel buttonPanel = new JPanel(new FlowLayout());
		JButton addButton = new JButton("Add");
		addButton.addActionListener((event) -> {
			String word = inputField.getText();
			wl.add(word);
			logField.setText(getTimeString() + "Added word " + word + " to list.");
			updateDisplay();
		});
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener((event) -> {
			logField.setText(getTimeString() + "Saved to wordlist.txt.");
			wl.save("wordlist.txt");
		});
		JButton loadButton = new JButton("Load");
		loadButton.addActionListener((event) -> {
			logField.setText(getTimeString() + "wordlist.txt loaded.");
			wl.load("wordlist.txt");
			updateDisplay();
		});
		buttonPanel.add(addButton);
		buttonPanel.add(saveButton);
		buttonPanel.add(loadButton);

		bottomPanel.add(buttonPanel, BorderLayout.CENTER);

		return bottomPanel;
	}

	private JPanel getTopPanel() {
		JPanel topPanel = new JPanel(new GridLayout(3, 1));

		JPanel inputPanel = new JPanel(new BorderLayout());

		inputField = new JTextField();
		inputField.addCaretListener((event) -> updateDisplay());
		inputField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent keyEvent) {

			}

			@Override
			public void keyPressed(KeyEvent keyEvent) {
				if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN) {
					if (anyField.isSelected()) {
						exactField.setSelected(true);
					} else if (exactField.isSelected()) {
						prefField.setSelected(true);
					} else {
						anyField.setSelected(true);
					}
				} else if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {
					if (anyField.isSelected()) {
						prefField.setSelected(true);
					} else if (exactField.isSelected()) {
						anyField.setSelected(true);
					} else {
						exactField.setSelected(true);
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent keyEvent) {

			}
		});
		inputPanel.add(inputField, BorderLayout.CENTER);

		JButton clearButton = new JButton("Clear");
		clearButton.addActionListener(event -> {
			inputField.setText("");
			inputField.requestFocus();
			prefField.setSelected(true);
			spaceField.setSelected(false);
		});
		inputPanel.add(clearButton, BorderLayout.EAST);

		topPanel.add(inputPanel);

		JPanel filtPanel = new JPanel(new GridLayout(1, 3));

		prefField = new JRadioButton("Prefix", true);
		prefField.addItemListener((event) -> updateDisplay());
		filtPanel.add(prefField);

		anyField = new JRadioButton("Any");
		anyField.addItemListener((event) -> updateDisplay());
		filtPanel.add(anyField);

		exactField = new JRadioButton("Exact");
		exactField.addItemListener((event) -> updateDisplay());
		filtPanel.add(exactField);

		ButtonGroup bg = new ButtonGroup();
		bg.add(anyField);
		bg.add(exactField);
		bg.add(prefField);

		topPanel.add(filtPanel);

		spaceField = new JCheckBox("Ignore space");
		spaceField.addItemListener((event) -> updateDisplay());
		topPanel.add(spaceField);

		return topPanel;
	}

	private void updateDisplay() {
		inputField.requestFocus();
		Filter fil = new Filter(inputField.getText());
		if (anyField.isSelected())
			fil.setAnyMatch();
		else if (exactField.isSelected())
			fil.setExactMatch();
		if (spaceField.isSelected())
			fil.setIgnoreSpace();
		dispList.setListData(wl.getFilteredList(fil));
	}

	private String getTimeString() {
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		String text = date.format(formatter);
		return "[" + text + "] ";
	}

}
